<?php
/**
 * Plugin Name:       Coding Ninjas Addon
 * Description:       Extends the functionality of the coding-ninjas plugin.
 * Version:           1.0
 * Author:            CodingNinjas
 * Text Domain:       cn-addon
 */

if ( ! defined( 'ABSPATH' ) ) {
    die;
}


if( ! class_exists( 'CN_Addon' ) ) {

    class CN_Addon {

        /** @var string The plugin version number */
        protected $version = '1.0';

        /** @var array Storage for class instances */
        protected static $instance;

        /** @var array Storage for class instances */
        protected $post_type = 'freelancer';

        /**
         * Constructor.
         */
        protected function __construct() {
            $version = $this->version;
            $path = plugin_dir_path( __FILE__ );
            $url = plugin_dir_url( __FILE__ );

            define( 'CN_Addon_VERSION', $version );
            define( 'CN_Addon_PATH', $path );
            define( 'CN_Addon_URL', $url );

            $this->actions();
        }

        /**
         * Return plugin instance.
         *
         * @return CN_Addon
         */
        protected static function get_instance() {
            return is_null( static::$instance ) ? new CN_Addon() : static::$instance;
        }

        /**
         * Initialize plugin.
         *
         * @return void
         */
        public static function run() {
            static::$instance = static::get_instance();
        }

        /**
         * Add plugin actions.
         *
         * @return void
         */
        public function actions() {
            register_activation_hook( __FILE__, array( $this,'parent_plugin_activate' ));
            add_action( 'init', array( $this, 'register_post_types' ) );
            add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
            add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
            add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_styles' ) );
            add_action( 'add_meta_boxes', array( $this, 'add_custom_cn_addon_fields' ) );
            add_action( 'save_post', array( $this, 'save_post' ), 10, 2 );

	        add_action( 'add_meta_boxes', array( $this, 'add_parent_cn_addon_fields' ) );
	        add_action('save_post', array( $this, 'parent_save_metabox' ), 10, 2 );

            add_shortcode('cn_addon', array( $this, 'shortcode_callback' ) );
            add_filter( 'manage_' . $this->post_type . '_posts_columns', array( $this, 'filter_posts_columns' ) );
            add_action( 'manage_' . $this->post_type . '_posts_custom_column', array( $this, 'posts_custom_column' ), 10, 2 );

            add_image_size( 'cn_addon_logo', 110, 33, true );
        }

        /**
         * For activate the Parent and Child plugin at the same time
         */
        function parent_plugin_activate(){
            // Require parent plugin
            if ( ! is_plugin_active( 'cn_php_wp_plugin_for_tasks/coding-ninjas.php' ) and current_user_can( 'activate_plugins' ) ) {
                // Stop activation redirect and show error
                wp_die('Sorry, but this plugin requires the Coding Ninjas Tasks Plugin to be installed and active. <br><a href="' . admin_url( 'plugins.php' ) . '">&laquo; Return to Plugins</a>');
            }
        }

        /**
         * Enqueue styles for frontend part.
         *
         * @return void
         */
        public function enqueue_styles() {

            wp_register_style( 'cn-addon', CN_Addon_URL . 'css/style.css' );
            wp_enqueue_style( 'cn-addon' );

        }

        /**
         * Enqueue admin scripts.
         *
         * @return void
         */
        public function admin_enqueue_scripts() {
            wp_enqueue_script( 'admin-cn-addon', CN_Addon_URL . 'js/admin.js', array( 'wp-color-picker' ), false, true );

        }

        /**
         * Enqueue admin styles.
         *
         * @return void
         */
        public function admin_enqueue_styles() {
            wp_enqueue_style( 'wp-color-picker' );

            wp_register_style( 'cn-addon-admin', CN_Addon_URL . 'css/admin.css' );
            wp_enqueue_style( 'cn-addon-admin' );

        }

        /**
         * Register plugin post types.
         *
         * @return void
         */
        public function register_post_types() {

            $labels = array(
                'name' => _x( 'Freelancer', 'post type general name', 'cn-addon' ),
                'singular_name' => _x( 'Freelancer', 'post type singular name', 'cn-addon' ),
                'add_new' => _x( 'Add New Freelancer', 'post type add new', 'cn-addon' ),
                'add_new_item' => __( 'Add New Freelancer', 'cn-addon' ),
                'edit_item' => __( 'Edit Freelancer', 'cn-addon' ),
                'new_item' => __( 'New Freelancer', 'cn-addon' ),
                'all_items' => __( 'All Freelancers', 'cn-addon' ),
                'view_item' => __( 'View Freelancer', 'cn-addon' ),
                'search_items' => __( 'Search Freelancer', 'cn-addon' ),
                'not_found' => __( 'No Freelancers found', 'cn-addon' ),
                'not_found_in_trash' => __( 'No Freelancers found in Trash', 'cn-addon' ),
                'parent_item_colon' => '',
                'menu_name' => __( 'Freelancer', 'cn-addon' ),
            );

            register_post_type( $this->post_type, array(
                'labels' => $labels,
                'public' => true,
                'show_ui' => true,
                'menu_icon' => 'dashicons-art',
                'capability_type' => 'post',
                'hierarchical' => false,
                'query_var' => true,
                'has_archive' => false,
                'supports' => array( 'title', 'thumbnail' )
            ));

        }

        /**
         * Add metabox to Freelancer post type.
         */
        public function add_custom_cn_addon_fields() {
            add_meta_box(
                'cn_addon_fields',
                'Freelancer fields',
                array( $this, 'cn_addon_fields_box_html' ),
                $this->post_type
            );

            add_meta_box(
                'cn_addon_shortcode',
                'Shortcode',
                array( $this, 'cn_addon_shortcode_box_html' ),
                $this->post_type,
                'side'
            );
        }

	    /**
	     * Add meta_box to Task post type.
	     */
	    public function add_parent_cn_addon_fields($post) {
			global $post;
	    	$classTask = new \codingninjas\Task($post);
		    $classTask_cpt = $classTask::POST_TYPE; //die;

		    add_meta_box(
			    'parent_cn_addon_meta_box',
			    'Freelancer',
			    array( $this, 'parent_element_grid_class_meta_box' ),
			    $classTask_cpt,
			    'side'
		    );
	    }

	    /**
	     * Add list of freelancers
	     * @param $post
	     */
        public function parent_element_grid_class_meta_box($post) {

	        $all_freelancers = array(
	        	'post_type' => $this->post_type,
	        	'posts_per_page' => -1,
		        );
	        $all_results = get_posts( $all_freelancers );
	        $freelancer = get_post_meta( $post->ID, 'parent_element_grid_class_meta_box', true ); //true ensures you get just one value instead of an array
	        ?>

			<select name="parent_element_grid_class" id="parent_element_grid_class">
				<?php foreach( $all_results as $result => $item_post ) :?>
			        <option value="<?php echo $item_post->ID;?>" <?php selected( $freelancer, $item_post->ID );?> ><?php echo $item_post->post_title;?></option>
				<?php endforeach;?>
			 </select>

	        <?php
        }

	    /**
	     * Save choose select Freelancer in Task
	     */
	    function parent_save_metabox() {
		    global $post;
		    if( isset( $_POST["parent_element_grid_class"] ) ){
			    update_post_meta($post->ID, 'parent_element_grid_class_meta_box', sanitize_text_field( $_POST['parent_element_grid_class'] ) );
			    //print_r($_POST);
		    }
	    }

        /**
         * Save post.
         * @param $post_id
         * @param $post
         */
        public function save_post( $post_id, $post ) {

            $post_type = get_post_type( $post_id );

            if ( $this->post_type != $post_type ) return;
            if ( ! empty( $_POST['banner'] ) ) {
                foreach ( $_POST['banner'] as $name => $value ) {
                    update_post_meta( $post_id, 'cn_addon_' . $name, sanitize_text_field( $value ) );
                }
            }
        }

	    /**
	     * Freelancer fields html.
	     * @param $post
	     */
	    public function cn_addon_fields_box_html( $post ) {
		    $banner_title = get_post_meta( $post->ID, 'cn_addon_title', true );
		    include CN_Addon_PATH . '/templates/admin/banner-fields.php';
	    }

	    public function cn_addon_shortcode_box_html( $post ) {
		    echo '[cn_addon id =' . $post->ID . ']';
	    }

        /**
         * Freelancer shortcode callback.
         * @param $atts
         * @return false|string
         */
        public function shortcode_callback( $atts ) {
            $attributes = shortcode_atts(
                array(
                    'id' => null,
                ),
                $atts
            );

            if ( ! empty( $attributes['id'] ) ) {
                $banner = get_post( $attributes['id'] );
                $banner_title = get_post_meta( $banner->ID, 'cn_addon_title', true );
                $banner_logo = get_the_post_thumbnail( $banner, 'cn_addon_logo' );
            }

            ob_start();
            $template = apply_filters( 'cn_addon_shortcode_template', CN_Addon_PATH . '/templates/shortcode.php' );
            include $template;
            return ob_get_clean();
        }

        /**
         * Add custom column to Freelancer post type.
         * @param $columns
         * @return mixed
         */
        public function filter_posts_columns( $columns ) {
            $columns['shortcode'] = __( 'Shortcode' );
            return $columns;
        }

        /**
         * @param $column
         * @param $post_id
         */
        public function posts_custom_column( $column, $post_id ) {
            if ( 'shortcode' === $column ) {
                echo '[cn_addon id=' . $post_id . ']';
            }
        }

    }

// Initialize plugin.
    CN_Addon::run();

} // class_exists check


