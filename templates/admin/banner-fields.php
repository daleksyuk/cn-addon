<?php
/**
 * Template file for cn addon fields in admin panel.
 */
?>
<div class="admin-input-holder">
    <label class="rt-field-label"><?php _e( 'Name', 'cn-addon' );?></label>
    <input name="banner[title]" value="<?php echo $banner_title;?>">
</div>