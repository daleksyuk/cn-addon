<?php
/**
 * Template file for cn_addon shortcode.
 */
?>
<?php if ( ! empty( $banner ) ):?>
    <div class="cn-addon">
        <?php if ( ! empty( $banner_title ) ):?>
            <div class="cn-col">
                <h3><?php echo $banner_title;?></h3>
            </div>
        <?php endif;?>
        <?php if ( ! empty( $banner_logo ) ):?>
            <div class="cn-col">
                <div class="cn-bottom-box">
                    <?php echo $banner_logo;?>
                </div>
            </div>
        <?php endif;?>
    </div>
<?php endif;